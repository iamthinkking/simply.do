#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  portrait.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#!/usr/bin/python3.4


#from gi.repository import Gtk, GObject, Gio
#import os.path
#import config
#import sys
#import simplyDBconnection

"""
class simply(Gtk.Window):
    file_path = ""
    clearbut = None
    todoitems = []
    vbox = None
    entry = None
    def __init__(self, fp):
        #window init
        Gtk.Window.__init__(self, title=config.name)

        #window settings
        self.set_border_width(7)
        self.set_default_size(300, 700)

        #windows size
        self.checkbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=3)
        self.add(self.checkbox)

        #header bar (hb) building
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = config.name
        self.set_titlebar(hb)

        scrolled = Gtk.ScrolledWindow()
        scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        #headerbar packing

        #right button set


        gears_btn = Gtk.MenuButton()

        gears = ["Options", "Quit"]
        gears_menu = Gtk.Menu()
        gears_btn.set_popup(gears_menu)
        i=0
        while i < len(gears):
            gear = Gtk.MenuItem("%s" % gears[i])
            if gears[i] == "Quit":
                gear.connect_object ("activate", self.destroy, "file.quit")
            gears_menu.append(gear)
            gear.show()
            i += 1

        gears_icon = Gio.ThemedIcon(name="application-menu-symbolic")
        gears_image = Gtk.Image.new_from_gicon(gears_icon, Gtk.IconSize.BUTTON)
        gears_btn.add(gears_image)
        gears_btn.set_tooltip_text("Gears")
        hb.pack_end(gears_btn)


        notehandle_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(notehandle_box.get_style_context(), "linked")

        import_btn = Gtk.MenuButton()
        import_icon = Gio.ThemedIcon(name="document-import-symbolic")
        import_image = Gtk.Image.new_from_gicon(import_icon, Gtk.IconSize.BUTTON)
        import_btn.add(import_image)
        import_btn.set_tooltip_text("import note")
        notehandle_box.add(import_btn)

        export_btn = Gtk.MenuButton()
        export_icon = Gio.ThemedIcon(name="document-export-symbolic")
        export_image = Gtk.Image.new_from_gicon(export_icon, Gtk.IconSize.BUTTON)
        export_btn.add(export_image)
        export_btn.set_tooltip_text("export note")
        notehandle_box.add(export_btn)


        hb.pack_end(notehandle_box)


        #left button set

        timedtask_btn = Gtk.Button()
        timedtask_icon = Gio.ThemedIcon(name="appointment-new-symbolic")
        timedtask_image = Gtk.Image.new_from_gicon(timedtask_icon, Gtk.IconSize.BUTTON)
        timedtask_btn.add(timedtask_image)
        timedtask_btn.set_tooltip_text("add task with deadline")
        hb.pack_start(timedtask_btn)

        tag_btn = Gtk.Button()
        tag_icon = Gio.ThemedIcon(name="tag-symbolic")
        tag_image = Gtk.Image.new_from_gicon(tag_icon, Gtk.IconSize.BUTTON)
        tag_btn.add(tag_image)
        tag_btn.set_tooltip_text("tagged tasks")
        hb.pack_start(tag_btn)

        #creating ControlBar
        controlbar = Gtk.HButtonBox()
        newcheckitem_btn = Gtk.Button()
        newcheckitem_icon = Gio.ThemedIcon(name="window-maximize-symbolic")
        newcheckitem_image = Gtk.Image.new_from_gicon(newcheckitem_icon, Gtk.IconSize.BUTTON)
        newcheckitem_btn.add(newcheckitem_image)
        newcheckitem_btn.set_tooltip_text("New CheckList Item")
        controlbar.pack_start(newcheckitem_btn, False, True, 0)
        newcheckitem_btn.connect("clicked", self.addtask)

        self.clear_btn = Gtk.Button()
        clear_icon = Gio.ThemedIcon(name="edit-clear-all-symbolic")
        self.clear_btn.connect("clicked", self.clear_checked)
        clear_image = Gtk.Image.new_from_gicon(clear_icon, Gtk.IconSize.BUTTON)
        self.clear_btn.add(clear_image)
        self.clear_btn.set_tooltip_text("Clear Checked Items")
        controlbar.pack_end(self.clear_btn, True, False, 0)

        self.newcheckitem_entry = Gtk.Entry()
        self.newcheckitem_entry.set_placeholder_text("add a new task here")
        self.newcheckitem_entry.connect('key-press-event', self.pushtodo)

        self.checkbox.pack_end(self.newcheckitem_entry, False, True, 5)
        self.checkbox.pack_end(controlbar, False, True, 0)

        #main stuff


        self.file_path = fp
        if(os.path.exists(self.file_path)):
            tdfile = open(self.file_path, "r+")
            tdfile_list = tdfile.read().split('\n')
            for todo in tdfile_list:
                self.addbutton(todo)
            tdfile.close()


        try:
            self.set_icon_from_file("data/icons/icon.png")
        except Exception as e:
            print (e.message)
            sys.exit(1)


    def newcheckitem_btn_clicked(self, widget):
        PointBox = simply()
        PointBox.newList()
        PointBox.getList(0)

    def pushtodo(self, widget, entry):
        if entry.keyval == 65293: #if enter is pressed
            self.addbutton(widget.get_text()) #get text from entry and add it to the label
            widget.set_text("") #text back text to blank
            return True
        return False

    def poptodo(self, widget):
        self.todoitems.remove(str(widget.get_label()))
        self.checkbox.remove(widget)
        if not self.todoitems:
            self.clear_btn.hide()

    def addbutton(self, label):
        if(label.strip() == ""):
            return
        if label.strip() not in self.todoitems:
            self.clear_btn.show()
            self.todoitems.append(label)
            button = Gtk.CheckButton(label)
            self.checkbox.add(button)
            print (self.todoitems)
            button.show()

    def addtask(self, widget):
        self.addbutton(self.newcheckitem_entry.get_text())

    def clear_checked(self, widget):
        for v in self.checkbox:
            if(type(v) == Gtk.CheckButton):
                if(v.get_active() == True):
                    self.poptodo(v)

    def destroy(self, event, param = None):
        tdfile = open(self.file_path, 'w')
        for v in self.todoitems:
            #if(type(v) == Gtk.CheckButton):
                #if(v.get_active() == False):
            tdfile.write(str(v) + "\n")
        tdfile.close()
        Gtk.main_quit()


def main2():
	win = simply(config.fp)
	win.connect("delete-event", win.destroy)
	win.show_all()
	Gtk.main()
"""
