#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
#
#  core/backendBase.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

""" Class that essentially provides a guide for what a backend needs to implement """
class BackendBase(object):
    def __init__(self):
        print("This is just the beginning")

    def save_note(self, note):
        #saves note object into the database
        print("Save Note")

    def load_note(self, note_id):
        #fetches note object from the database
        print("Load Note")

    def get_timestamp(self, note_id):
        #returns timestamp
        print("Get Timestamp")

    def set_timestamp(self, note_id, timestamp):
        #set timestamp on note
        print("Set Timestamp")

    def get_title(self, note_id):
        #returns title
        print("Get Title")

    def set_title(self, note_id, title):
        #set title
        print("Set Title")

    def get_content(self, note_id):
        #returns content
        print("Get Content")

    def set_content(self, note_id, content):
        #sets content
        print("Set Content")

    def get_tags(self, note_id):
        #returns list of all tags
        print("Get Tags")

    def add_tag(self, note_id, tag):
        #adds tag
        print("Add Tag")

    def remove_tag(self, note_id, tag):
        #removes tag
        print("Remove Tag")

    def get_priority(self, note_id):
        #get priority
        print("Get Priority")

    def set_priority(self, note_id, priority):
        #set priority
        print("Set Priority")
