#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
#
#  core/abstractDatabase.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import os.path
from simply.backend.localBackend import LocalBackend

class AbstractDatabase(object):
    """ Handles all other database objects and communicates with the gui """

    current_sidebar_data = [] #an array the holds the current sidebar data

    def __init__(self):
        print("Abstract Database")
        self.load_backends()

    def load_backends(self):
        b = LocalBackend()

    #returns an array of note data objects
    def get_sidebar_data(self):
        self.current_sidebar_data = []

        test = [Priority.CRITICAL, Priority.HIGH, Priority.MEDIUM, Priority.LOW, Priority.NONE]
        if(os.path.exists(config.FILE_PATH)):
            tdfile = open(config.FILE_PATH, "r+")
            tdfile_list = tdfile.read().split('\n')
            i = 0
            for todo in tdfile_list:
                if todo != "":
                    summ = "This is a fancy summary that is very long and because it is so long it will go beyond the limit of the sidebar"
                    note = NoteData(0, 0, todo, summ, "Jan 31", test[i], [])
                    self.current_sidebar_data.append(note)
                i += 1
                if (i >= len(test)): i = 0
            tdfile.close()
        else:
            print("Unable to read %s" % config.FILE_PATH)

        return self.current_sidebar_data

class NoteData(object):
    """ A data abstraction class for easily accessing note attributes """
    #Note information variables
    note_id = None
    notebook_id = None
    title = None
    summary = None
    date = None
    priority = None
    tags = []

    def __init__(self, note_id, notebook_id, title, summary, date, priority, tags):
        #set global variables
        self.note_id = note_id
        self.notebook_id = notebook_id
        self.title = title
        self.summary = summary
        self.date = date
        self.priority = priority
        self.tags = tags
