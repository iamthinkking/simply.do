#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
#
#  widgets/noteEditor.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from gi.repository import Gtk, GObject, Gio, GtkSource, Pango
import simply.config as config

class Editor(Gtk.Box):
    def __init__(self):
        super(Editor, self).__init__()

        self.setup_layout()

        self.show_all()

    def setup_layout(self):
        editor = NoteEditor()
        self.pack_start(editor, True, True, 0)

class NoteEditor(GtkSource.View):
    def __init__(self):
        super(NoteEditor, self).__init__()

        self.set_wrap_mode(Gtk.WrapMode.WORD_CHAR);
        self.props.smart_home_end = GtkSource.SmartHomeEndType.AFTER;
        self.props.margin = 5;
