# simply.do - note taking application
Application in active development

![alt text][screen]

[screen]: https://gitlab.com/thinkking/simply.do/raw/master/screens/Screenshot_2016-03-24_10-00-19.png


## Installation
To install locally run the following commands:

    mkdir build && cd build
    cmake ..
    make
    sudo make install

Note that this currently doesn't check for the required libraries.

To uninstall run the following commands from within the build folder.

    sudo make uninstall

## Notes for running locally
To launch the application using local css files and databases
(for faster development), launch simply with the --debug
option.

    python3 simplydo --debug

or simply run the included debug script

    ./debug.sh

Because the program uses gsettings and schemas, simply must
be installed at least once with the above instructions.
Any changes to the schemas will need a re-installation.

## Translation
Not yet implemented

## Hot Pics
None available

### Contributors (github usernames)
-@thinkking
-@sgpthomas
-@bil-elmoussaoui
-@aniket-deole
